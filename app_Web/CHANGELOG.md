## 修复请参考主模板，目前于2020-06-14合并master。[链接](http://demo.ibizlab.cn/ibizr7pfstdtempl/ibizvuer7plus/blob/master/CHANGELOG.md)

## 0.0.1-alpha.1「2020-06-14」

### 功能新增

> 首页支持视图界面样式「STYLE2」，右侧导航菜单 + 无导航分页路由模式。