/**
 * AssignToMe 部件模型
 *
 * @export
 * @class AssignToMeModel
 */
export default class AssignToMeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof AssignToMeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'canceledby',
      },
      {
        name: 'left',
      },
      {
        name: 'openeddate',
      },
      {
        name: 'color',
      },
      {
        name: 'task',
        prop: 'id',
      },
      {
        name: 'finishedby',
      },
      {
        name: 'finishedlist',
      },
      {
        name: 'realstarted',
      },
      {
        name: 'closedby',
      },
      {
        name: 'substatus',
      },
      {
        name: 'closedreason',
      },
      {
        name: 'lastediteddate',
      },
      {
        name: 'assigneddate',
      },
      {
        name: 'pri',
      },
      {
        name: 'lasteditedby',
      },
      {
        name: 'status',
      },
      {
        name: 'name',
      },
      {
        name: 'closeddate',
      },
      {
        name: 'type',
      },
      {
        name: 'assignedto',
      },
      {
        name: 'desc',
      },
      {
        name: 'eststarted',
      },
      {
        name: 'deadline',
      },
      {
        name: 'deleted',
      },
      {
        name: 'mailto',
      },
      {
        name: 'consumed',
      },
      {
        name: 'estimate',
      },
      {
        name: 'openedby',
      },
      {
        name: 'canceleddate',
      },
      {
        name: 'finisheddate',
      },
      {
        name: 'modulename',
      },
      {
        name: 'storyname',
      },
      {
        name: 'projectname',
      },
      {
        name: 'product',
      },
      {
        name: 'storyversion',
      },
      {
        name: 'productname',
      },
      {
        name: 'parentname',
      },
      {
        name: 'project',
      },
      {
        name: 'story',
      },
      {
        name: 'parent',
      },
      {
        name: 'frombug',
      },
      {
        name: 'duration',
      },
      {
        name: 'module',
      },
      {
        name: 'path',
      },
      {
        name: 'comment',
      },
      {
        name: 'currentconsumed',
      },
      {
        name: 'totaltime',
      },
    ]
  }


}
