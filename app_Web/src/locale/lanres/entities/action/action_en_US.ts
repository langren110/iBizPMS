
export default {
  fields: {
    extra: "附加值",
    objecttype: "对象类型",
    id: "id",
    comment: "备注",
    read: "已读",
    action: "动作",
    date: "日期",
    product: "产品",
    objectid: "对象ID",
    actor: "操作者",
    project: "项目",
  },
	views: {
		historylistview: {
			caption: "系统日志",
      		title: "历史记录",
		},
		projecttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		projecttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		producttrendslistview9: {
			caption: "系统日志",
      		title: "产品动态",
		},
		alltrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		producttrendslistview: {
			caption: "系统日志",
      		title: "系统日志列表视图",
		},
		editview: {
			caption: "系统日志",
      		title: "action编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "action基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "id", 
			srfmajortext: "备注", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "id", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};
