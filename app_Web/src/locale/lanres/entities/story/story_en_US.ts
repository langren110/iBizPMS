
export default {
  fields: {
    assignedto: "指派给",
    childstories: "细分需求",
    plan: "所属计划",
    version: "版本号",
    assigneddate: "指派日期",
    pri: "优先级",
    linkstories: "相关需求",
    status: "当前状态",
    estimate: "预计工时",
    revieweddate: "评审时间",
    title: "需求名称",
    sourcenote: "来源备注",
    reviewedby: "由谁评审",
    substatus: "子状态",
    stagedby: "设置阶段者",
    openedby: "由谁创建",
    openeddate: "创建日期",
    id: "编号",
    source: "需求来源",
    closedreason: "关闭原因",
    color: "标题颜色",
    mailto: "抄送给",
    deleted: "已删除",
    keywords: "关键词",
    lasteditedby: "最后修改",
    stage: "所处阶段",
    closeddate: "关闭日期	",
    closedby: "由谁关闭",
    type: "需求类型",
    lastediteddate: "最后修改日期",
    path: "模块路径",
    parentname: "父需求名称",
    modulename: "所属模块名称",
    productname: "产品名称",
    frombug: "来源Bug",
    parent: "父需求",
    module: "所属模块",
    product: "所属产品",
    duplicatestory: "重复需求ID",
    branch: "平台/分支",
    tobug: "转Bug",
    spec: "需求描述",
    verify: "验收标准",
  },
	views: {
		reportsubgridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		mainview: {
			caption: "需求",
      		title: "需求数据看板视图",
		},
		editview: {
			caption: "需求",
      		title: "story编辑视图",
		},
		gridview9_assignedtome: {
			caption: "需求",
      		title: "需求表格视图",
		},
		mainview9_editmode: {
			caption: "需求",
      		title: "需求编辑视图",
		},
		plansubeditview: {
			caption: "需求",
      		title: "需求",
		},
		pickupgridview: {
			caption: "需求",
      		title: "需求",
		},
		buildsubgridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		mainview9_storyspec: {
			caption: "需求",
      		title: "需求编辑视图",
		},
		curprojectgridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		mainview_editmode: {
			caption: "需求",
      		title: "需求数据看板视图",
		},
		releasesubgridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		releasesubeditview: {
			caption: "需求",
      		title: "需求",
		},
		mainview9: {
			caption: "需求",
      		title: "需求编辑视图",
		},
		gridview9_substory: {
			caption: "子需求",
      		title: "需求表格视图",
		},
		maingridview_bymodule: {
			caption: "需求",
      		title: "story表格视图",
		},
		maingridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		mpickupview: {
			caption: "关联需求",
      		title: "关联需求",
		},
		plansubgridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		main2gridview: {
			caption: "需求",
      		title: "story表格视图",
		},
		editview_storychange: {
			caption: "变更",
      		title: "需求编辑视图",
		},
	},
	storyspec_editmode_form: {
		details: {
			grouppanel1: "分组面板", 
			druipart1: "", 
			grouppanel2: "分组面板", 
			group1: "需求描述信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			version: "版本#", 
			reviewedby: "由谁评审", 
			notreview: "不需要评审", 
			title: "需求名称", 
			verify: "验收标准", 
			spec: "需求描述", 
			comment: "备注", 
			files: "附件", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	main_editmode_form: {
		details: {
			group1: "story基本信息", 
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			group2: "需求的一生", 
			grouppanel3: "其他相关", 
			button1: "Save And Close", 
			grouppanel4: "操作按钮", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			title: "需求名称", 
			prodoctname: "所属产品", 
			modulename: "所属模块", 
			plan: "所属计划", 
			source: "需求来源", 
			sourcenote: "来源备注", 
			status: "当前状态", 
			stage: "所处阶段", 
			pri: "优先级", 
			estimate: "预计工时", 
			keywords: "关键词", 
			mailto: "抄送给", 
			openedby: "由谁创建", 
			openeddate: "于", 
			assignedto: "指派给", 
			assigneddate: "于", 
			linkstories: "相关需求", 
			id: "编号", 
			product: "所属产品", 
			module: "所属模块", 
		},
		uiactions: {
        saveandexit: "Save And Close",
		},
	},
	main_newmode_form: {
		details: {
			group1: "需求基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			product: "所属产品", 
			module: "所属模块", 
			prodoctname: "所属产品", 
			modulename: "所属模块", 
			plan: "计划", 
			source: "需求来源", 
			sourcenote: "来源备注", 
			reviewedby: "由谁评审", 
			title: "需求名称", 
			pri: "优先级", 
			estimate: "预计", 
			formitem: "需求描述", 
			formitem1: "验收标准", 
			mailto: "抄送给", 
			keywords: "关键词", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	storyspec_form: {
		details: {
			grouppanel1: "需求描述", 
			grouppanel2: "验收标准", 
			group1: "需求描述信息", 
			druipart1: "", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			parent: "父需求", 
			version: "版本#", 
			spec: "需求描述", 
			verify: "验收标准", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "story基本信息", 
			formpage1: "基本信息", 
			grouppanel1: "分组面板", 
			grouppanel2: "分组面板", 
			grouppanel3: "分组面板", 
			group2: "操作信息", 
			formpage2: "需求的一生", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			title: "需求名称", 
			prodoctname: "所属产品", 
			modulename: "所属模块", 
			plan: "所属计划", 
			source: "需求来源", 
			sourcenote: "来源备注", 
			status: "当前状态", 
			stage: "所处阶段", 
			pri: "优先级", 
			estimate: "预计工时", 
			keywords: "关键词", 
			mailto: "抄送给", 
			openedby: "由谁创建", 
			openeddate: "于", 
			assignedto: "指派给", 
			assigneddate: "于", 
			reviewedby: "由谁评审", 
			revieweddate: "评审时间", 
			closedby: "由谁关闭", 
			closeddate: "关闭日期	", 
			closedreason: "关闭原因", 
			lasteditedby: "最后修改", 
			lastediteddate: "于", 
			id: "编号", 
			product: "所属产品", 
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			assignedto: "指派给",
			estimate: "预计工时",
			status: "当前状态",
			uagridcolumn1: "操作",
		},
		uiactions: {
        story_changestorydetail: "变更",
        story_closestory: "关闭",
        story_openbaseinfoeditview: "编辑",
        story_opencasecreateview: "建用例",
		},
	},
	main_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			modulename: "所属模块名称",
			uagridcolumn1: "操作",
		},
		uiactions: {
        story_changestorydetail: "变更",
        story_closestory: "关闭",
        story_openbaseinfoeditview: "编辑",
        story_opencasecreateview: "建用例",
		},
	},
	pickupgrid_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			modulename: "所属模块名称",
		},
		uiactions: {
		},
	},
	main_plansub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			modulename: "所属模块名称",
		},
		uiactions: {
		},
	},
	main_buildsub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			modulename: "所属模块名称",
		},
		uiactions: {
		},
	},
	main_releasesub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			modulename: "所属模块名称",
		},
		uiactions: {
		},
	},
	main_reportsub_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			openedby: "创建",
			assignedto: "指派",
			estimate: "预计",
			status: "状态",
			stage: "阶段",
		},
		uiactions: {
		},
	},
	main9_grid: {
		columns: {
			pri: "P",
			title: "需求名称",
			status: "状态",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			id: "ID",
			pri: "P",
			title: "需求名称",
			uagridcolumn1: "操作",
		},
		uiactions: {
        story_changestorydetail: "变更",
        story_closestory: "关闭",
        story_openbaseinfoeditview: "编辑",
        story_opencasecreateview: "建用例",
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview_storychangetoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	maingridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "Edit",
			tip: "Edit {0}",
		},
	},
	maingridview_bymoduletoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "Edit",
			tip: "Edit {0}",
		},
	},
	plansubgridviewtoolbar_toolbar: {
		deuiaction3_planrelationstory: {
			caption: "关联需求",
			tip: "关联需求",
		},
		seperator2: {
			caption: "",
			tip: "",
		},
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		deuiaction4: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	plansubeditviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	curprojectgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "Edit",
			tip: "Edit {0}",
		},
	},
	buildsubgridviewtoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	releasesubeditviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	releasesubgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		deuiaction4: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	reportsubgridviewtoolbar_toolbar: {
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	main2gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem21: {
			caption: "Export Data Model",
			tip: "导出数据模型",
		},
		tbitem23: {
			caption: "数据导入",
			tip: "数据导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
};
