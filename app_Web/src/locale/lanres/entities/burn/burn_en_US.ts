
export default {
  fields: {
    date: "日期",
    id: "虚拟主键",
    consumed: "总计消耗",
    left: "预计剩余",
    estimate: "最初预计",
    project: "所属项目",
    task: "任务",
  },
	views: {
		chartview: {
			caption: "燃尽图",
      		title: "燃尽图",
		},
	},
};
